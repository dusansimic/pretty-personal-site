const {parallel, src, dest} = require('gulp')
const htmlmin = require('gulp-htmlmin')
const webp = require('gulp-webp')

function minifyHTML() {
	return src('src/*.html')
		.pipe(htmlmin({
			collapseBooleanAttributes: true,
			conservativeCollapse: true,
		}))
		.pipe(dest('public'))
}

function convertWebP() {
	return src('src/*.jpeg')
		.pipe(webp())
		.pipe(dest('public'))
}

exports.default = parallel(minifyHTML, convertWebP)
